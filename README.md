# Steps on installing archlinux to a uefi physical machine

### BIOS check: If "Secure Boot" option is enabled?
Most of our machines preinstalled with micro$oft. They enabled "secure boot" option so that you can't boot your machine with an usb stick. Open BIOS and disable "secure boot" option.

In my case F9/F12 pops up boot menu. When boot nemu comes, select USB drive

##3 Change keyboard layout
```bash
# loadkeys trq
```

### Confirm is your system legacy boot or efi?
```bash
# ls /sys/firmware/efi/efivars
```
After this command, if you see abunch of files, you can proceed your installation with UEFI with a GPT partition table.

## Network Configuration
```bash
# ping -c 4 8.8.8.8
```
If you can ping a host you are connected. In my case i need to set static ip:

### Network Configuration: Set Static IP
You can use `netctl` or `systemd` to set static ip. I will use `systemd`. First learn the name of our physical network 
```bash
# ip addr
or
# ip link
```
Sample output
```bash
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1
 link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: eno1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
 link/ether 08:00:27:db:14:7a brd ff:ff:ff:ff:ff:ff
```
Let's set static ip to name eno1:
```bash
# vim /etc/systemd/network/eno1.network
```
Write down the following:
```
[Match]
Name=eno1

[Network]
Address=192.168.1.102/16
Gateway=192.168.1.1
DNS=8.8.8.8
DNS=8.8.4.4
```

### Change nameserver
In my case i need to edit resolve.conf and set a static name server; otherwise i cannot connect to internet
```bash
# vim /etc/resolve.conf
and enter at the end:
nameserver 8.8.8.8
nameserver 8.8.4.4 #optional
```
After intalling a network manager, it will overrides resolve.conf.

### Check any network services overriding systemd?
```bash
# systemctl list-unit-files
```

If yes disable and stop that services for example if netctl is working:
```bash
# systemctl stop netctl@enp0s3.service
# systemctl disable netctl@enp0s3.service
or dhcpcd
# systemctl stop dhcpcd
# systemctl disable dhcpcd
```

Then enable and start systemd:
```bash
# systemctl enable systemd-networkd
# systemctl start systemd-networkd
```

Check the connection again

## Check system time
```bash
# timedatectl status
# timedatectl set-ntp true
```

## How to disk partition?
```bash
# fdisk -l
or 
# lsblk
then enter
# cfdisk
```

Choose GPT for UEFI systems<br />
/dev/sda1 -> 512M -> EFI System<br />
/dev/sda2 -> 4608M -> Linux Swap<br />
/dev/sda3 -> xxxG -> Linux filesystem<br />

Select "Write", and enter "yes", then quit<br />
On the terminal wite these commands to format partitions:

```bash
# mkfs.fat -F32 /dev/sda1
# mkfs.ext4 /dev/sda3
# mkswap /dev/sda2
# swapon /dev/sda2
```

### Mount file system
```bash
# mount /dev/sda3 /mnt
```

## Installation
I don't select mirror.
### Install the base packages
```bash
# pacstrap -i /mnt base base-devel vim zsh bash-completion wget
```

### Generate fstab file
```bash
# genfstab -U -p /mnt >> /mnt/etc/fstab
```

### Chroot to the installed system:
```bash
# arch-chroot /mnt 
```

### Set time zone
To login Gitlab you must use right timezone!
```bash
# ln -sf /usr/share/zoneinfo/RegionCity /etc/localtime
```

Now set the hardware clock from the system clock:
```bash
# hwclock --systohc
or
# hwclock -systohc --utc
```

### Set Locale
Uncomment your language
```bash
# vim /etc/locale.gen
```

Now generate the locale:
```bash
# locale-gen
```

Create locale.conf
```bash
# vim /etc/locale.conf
or :
# echo LANG=en_US.UTF-8 > /etc/locale.conf
# export LANG=en_US.UTF-8
# echo KEYMAP=trq > /etc/vconsole.conf
```

### Set Hostname
```bash
# echo archpc > /etc/hostname
```

Also we need to  edit hosts file:
```bash
# vim /etc/hosts
```

And enter:
```
# Static table lookup for hostnames.
# See hosts(5) for details.
127.0.0.1 localhost
::1 localhost
127.0.0.1 archpc.localdomain archpc
```

## Set Root Password
```bash
# passwd
```

### Install Grub
```bash
# mkdir /boot/efi
# mount /dev/sda1 /boot/efi
# pacman -S grub efibootmgr 
# grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi --recheck
# grub-mkconfig -o /boot/grub/grub.cfg
```

We have to make some tweaks for using grub:
```bash
# mkdir /boot/efi/EFI/BOOT
# cp /boot/efi/EFI/GRUB/grubx64.efi /boot/efi/EFI/BOOT/BOOTX64.EFI
# vim /boot/efi/startup.nsh
```

Enter these two line below to our startup script:
```bash
bcfg boot add 1 fs0:\EFI\GRUB\grubx64.efi "My GRUB bootloader"
exit
```

## Rebooting
```bash
# exit
# umount -R /mnt
# reboot
```

## After reboot:
### Activate multilib repo
Multilib repo have apps like wine or steam.
```bash
# vim /etc/pacman.conf
```

Just delete the `hashtags` infront of the line:
```bash
[multilib]
include = /etc/pacman.d/mirrorlist
```

### Add personal account
```bash
# useradd -m -g users -G audio,video,network,wheel,storage -s /bin/bash myusername
# passwd myusername
# visudo
```

After visudo delete the hastag in front of the line below:
```
%wheel ALL=(ALL) ALL
```

Exit and log on with your user credentials; then:
```bash
$ sudo pacman -Syu
```

## Xorg and XFCE
I am using XFCE. It is lightweight. We need to install Xorg to use XFCE:
```bash
$ sudo pacman -S xorg-server xorg-apps xorg-xinit xterm
```

### Graphics driver
Check which hardware do you have:
```bash
$ lspci | grep -e VGA -e 3D
```

Also check Xorg page: https://wiki.archlinux.org/index.php/Xorg<br />
It has nice information. In my case i have intel graphic card; so:
```bash
$ sudo pacman -S xf86-video-intel mesa
```

### Install display manager
I am using lightdm
```bash
$ sudo pacman -S lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings
$ sudo systemctl enable lightdm.service
```

### Install XFCE
```bash
$ sudo pacman -S xfce4 xfce4-goodies
```

### Install virt-manager properly
First check hardware support:
```bash
$ LC_ALL=C lscpu | grep Virtualization
```

If your computer supports virtualization, you should see the output as “Virtualization: VT-x” or “Virtualization: AMD-V”.
```bash
$ sudo pacman -S virt-manager qemu vde2 ebtables dnsmasq bridge-utils openbsd-netcat
$ sudo systemctl enable libvirtd.service
$ sudo systemctl start libvirtd.service
```

## Appereance
Let's make some tweaks to our arch setup
### Install top programming fonts
I download this beacouse i use menlo font
```bash
$ curl -L https://github.com/hbin/top-programming-fonts/raw/master/install.sh | bash
```

### Install oh-my-zsh
```bash
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

### Install aur wrapper : yay
```bash
$ sudo pacman -S git
$ git clone https://aur.archlinux.org/yay.git
$ cd yay
$ makepkg -si
```

### Theming - Style
```bash
$ yay -S gtk-theme-numix-solarized
```

### Theming - Icons
```bash
yay -S menda-circle-icon-theme-git
```
### Theme - Window Theme
```bash
yay -S osx-arc-darker
```

### Install Plank Dock and Theme
```bash
$ pacman -S plank
$ plank &
```

Logout and log on again
### Install NetworkManager and applet
```bash
$ sudo pacman -S networkmanager network-manager-applet nm-connection-editor
$ nm-applet &
$ sudo systemctl enable NetworkManager.service
$ sudo systemctl disable dhcpcd.service
$ sudo systemctl start NetworkManager.service
$ sudo reboot
```

### Install usb mount tools
```bash
$ sudo pacman -S ntfs-3g udisks2 udiskie
$ udiskie --tray &
```

### Install Archiver
```bash
$ sudo pacman -S engrampa
```

### Install decoder for TS videos

```txt
sudo pacman -S aribb24
```

### To change the username in the panel action buttons
```bash
$ sudo usermod -c "My name" myusername
$ xfce4-panel -r
```

### Sound configuration
```bash
$ sudo pacman -S alsa-utils
$ yay -Ss xfce4-mixer
```

### Blinking/Flashing Lightdm GTK Greeter:
```
Section "Device"
   Identifier  "Intel Graphics"
   Driver      "intel"
   Option      "AccelMethod" "uxa"
EndSection
```

### To fix resolution problem on lightdm greeter
I am using XFCE + Ligthdm + Lightdm-gtk-greeter. My desktop screen resolution is 1280x800 but lightdm greeter login screen is 800x600 or something like that. To fix this first run:
```bash
$ xrandr -q
```

According to the output edit this line in /etc/lightdm/lightdm.conf
```bash
$ sudo vim /etc/lightdm/lightdm.conf
this line:
#display-setup-script=
to:
display-setup-script=xrandr --output Virtual-1 --primary --mode 1280x800
```

### lsusb shows usb device but fdisk -l doesn't
Be sure that your system updated and rebooted.
The commands below must show the same version of arch linux
```bash
$ pacman -Q linux    
$ uname -r
```

### Vim Instant Markdown
```bash
$ yay -S vim-instant-markdown
```

### Delete/Clean Pacman Cache
How many cached packages in my cache folder:
```bash
$ sudo ls /var/cache/pacman/pkg/ | wc -l
```

how much disk space do these packages hold?
```bash
$ du -sh /var/cache/pacman/pkg/
```

To clean most recent 3 packages and delete the rest:
```bash
$ sudo paccache -r
```

More on this topic you can use this command to delete all the packages(but it is not recomended. you might need  to downgrade somehow):
```bash
$ sudo pacman -Sc
```

You can also use `yay`
```bash
$ yay -Sc --aur
```

### Conflicting files or Owned by <packagename> error during updating
```
error: failed to commit transaction (conflicting files)
libmemcached: /usr/share/man/man1/memdump.1.gz exists in filesystem (owned by memdump)
Errors occurred, no packages were upgraded.
```

Thant means pacman database for a single package is corrupted. So we can solve this issue by:
```bash
$ sudo pacman -U --dbonly /var/cache/pacman/pkg/libmemcached-1.0.18-4-x86_64.pkg.tar.zst
```
Then update by `sudo pacman -Syu`

### Searching Pacman database for a file
This command parameter is more like PisiLinux `pisi sf file` command. We can search for a file with this command
```bash
# update the database
$ sudo pacman -Fy

# search which package contains file
$ pacman -F $filename

# search for exact name or full Address
$ pacman -Fx $expr
```

Alternative: use `pkgfile`
```
# install
$ sudo pacman -S pkgfile

# update
$ sudo pkgfile -u

$ pkgfile $filename
```

## Deployment

You need arch linux or arch linux based manjaro to apply these notes

## Authors

* **Blue DeviL** - *Reverser* - [bluedevil](http://gitlab.com/bluedevil)

**HomePage** - [SCTZine](http://www.sctzine.com)

## License

This project is under the MIT License
